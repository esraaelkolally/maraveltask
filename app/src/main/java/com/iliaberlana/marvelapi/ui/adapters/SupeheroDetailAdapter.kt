package com.iliaberlana.marvelapi.ui.adapters

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.iliaberlana.marvelapi.R
import com.iliaberlana.marvelapi.ui.commons.inflate
import com.iliaberlana.marvelapi.ui.model.MarvelSuperheroeForList
import com.iliaberlana.marvelapi.ui.presenters.DetailPresenter
import com.iliaberlana.marvelapi.ui.presenters.MainPresenter

class SupeheroDetailAdapter (
        private val presenter: DetailPresenter,
        val fetchNewPage: () -> Unit
    ) : RecyclerView.Adapter<SuperheroesDetailsViewHolder>() {

        private var distance: Int = 6
        private var waitingForNextPage: Boolean = false

        private var marvelSuperheroes: MutableList<MarvelSuperheroeForList> = ArrayList()

        fun addAll(collection: Collection<MarvelSuperheroeForList>) {
            setWaitingForNextPageFalse()
            marvelSuperheroes.addAll(collection)
            notifyDataSetChanged()
        }

        fun clean() {
            setWaitingForNextPageFalse()
            marvelSuperheroes.clear()
            notifyDataSetChanged()
        }

        override fun getItemViewType(position: Int): Int {
            if (!waitingForNextPage) {
                if (position.plus(distance) >= itemCount) {
                    setWaitingForNextPageTrue()
//                    fetchNewPage()
                }
            }

            return super.getItemViewType(position)
        }

        private fun setWaitingForNextPageFalse() {
            waitingForNextPage = false
        }

        private fun setWaitingForNextPageTrue() {
            waitingForNextPage = true
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SuperheroesDetailsViewHolder =
            SuperheroesDetailsViewHolder(parent.inflate(R.layout.superheroe_item), presenter)

        override fun onBindViewHolder(holder: SuperheroesDetailsViewHolder, position: Int) =
            holder.bind()
//            holder.bind(marvelSuperheroes[position])

//        override fun getItemCount(): Int = marvelSuperheroes.size
        override fun getItemCount(): Int = 6
    }

